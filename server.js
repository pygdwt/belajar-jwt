const express = require("express");
const app = express();
const passport = require("./lib/passport");
const router = require("./router");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(passport.initialize());

app.use(router);

app.listen(8000, () => {
  console.log("Server is running on port 8000");
});
