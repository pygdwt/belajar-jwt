const router = require("express").Router();

const restrict = require("./middlewares/restrict");

const auth = require("./controllers/authController");

router.get("/", (req, res) => {
  res.json({
    status: "Belajar JWT",
  });
});

router.post("/register", auth.register);
router.post("/login", auth.login);

module.exports = router;
