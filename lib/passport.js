const passport = require("passport");
const { Strategy: JwtStratgy, ExtractJwt } = require("passport-jwt");

const { User } = require("../models");

const options = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),

  secretOrKey: "Rahasia",
};

passport.use(
  new JwtStratgy(options, async (payload, done) => {
    User.findByPk(payload.id)
      .then((user) => done(null, user))
      .catch((err) => done(err, false));
  })
);

module.exports = passport;
